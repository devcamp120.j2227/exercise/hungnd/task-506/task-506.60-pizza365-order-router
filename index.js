
// Import thư viện Express Js
const express = require("express");
const {drinkRouter} = require('./app/routes/drinkRouter');
const {voucherRouter} = require('./app/routes/voucherRouter');
const {userRouter} = require('./app/routes/userRouter');
const {orderRouter} = require('./app/routes/orderRouter');
// Khởi tạo 1 app express
const app = express();

// Khai báo 1 cổng 
const port = 8000;

app.use('/', (req, res, next) => {
    console.log(new Date());

    next();
});
// gọi drink router
app.use('/', drinkRouter);
// gọi voucher router
app.use('/', voucherRouter);
// gọi user router
app.use('/', userRouter);
// gọi order router
app.use('/', orderRouter);
// run app on declared port
app.listen(port, () => {
    console.log(`App running on port ${port}`);
});