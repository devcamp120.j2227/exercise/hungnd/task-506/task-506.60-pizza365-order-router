
// khai báo các middleware
const getAllDrinkMiddleware = (req, res, next) => {
    console.log("Get All Drink");
    next();
};

const getDrinkMiddleware = (req, res, next) => {
    console.log("Get a Drink");
    next();
};

const postDrinkMiddleware = (req, res, next) => {
    console.log("Create a Drink");
    next();
};

const putDrinkMiddleware = (req, res, next) => {
    console.log("Update a Drink");
    next();
};

const deleteDrinkMiddleware = (req, res, next) => {
    console.log("Delete a Drink");
    next();
};

module.exports = {
    getAllDrinkMiddleware,
    getDrinkMiddleware,
    postDrinkMiddleware,
    putDrinkMiddleware,
    deleteDrinkMiddleware
}
