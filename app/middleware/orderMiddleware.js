// khai báo các middleware
const getAllOrderMiddleware = (req, res, next) => {
    console.log("Get All Order");
    next();
};

const getOrderMiddleware = (req, res, next) => {
    console.log("Get a Order");
    next();
};

const postOrderMiddleware = (req, res, next) => {
    console.log("Create a Order");
    next();
};

const putOrderMiddleware = (req, res, next) => {
    console.log("Update a Order");
    next();
};

const deleteOrderMiddleware = (req, res, next) => {
    console.log("Delete a Order");
    next();
};
module.exports = {
    getAllOrderMiddleware,
    getOrderMiddleware,
    postOrderMiddleware,
    putOrderMiddleware,
    deleteOrderMiddleware
}