// Import thư viện Express Js
const express = require("express");
// khai báo các middleware user
const {
    getAllUserMiddleware,
    getUserMiddleware,
    postUserMiddleware,
    putUserMiddleware,
    deleteUserMiddleware
} = require('../middleware/userMiddleware');
// tạo ra userRouter 
const userRouter = express.Router();

// Khởi tạo các phương thức với middle voucher
userRouter.get('/users', getAllUserMiddleware, (req, res) => {
    res.json({
        message: "Get all users"
    })
});
// phương thức post
userRouter.post('/vouchers', postUserMiddleware, (req, res) => {
    res.json({
        message: "Create a users"
    })
});
// phương thức get drink by Id
userRouter.get('/users/:userId', getUserMiddleware, (req, res) => {
    let userId = req.params.userId;
    res.json({
        message: `Get a users by Id = ${userId}`
    })
});
// phương thức put drink by Id
userRouter.put('/users/:userId', putUserMiddleware, (req, res) => {
    let userId = req.params.userId;
    res.json({
        message: `Update a users by Id = ${userId}`
    })
});
// phương thức delete drink by Id
userRouter.delete('/users/:userId', deleteUserMiddleware, (req, res) => {
    let userId = req.params.userId;
    res.json({
        message: `Delete a users by Id = ${userId}`
    })
});
module.exports = {userRouter};
