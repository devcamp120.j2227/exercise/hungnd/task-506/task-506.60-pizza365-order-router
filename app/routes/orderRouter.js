// khai báo thư viện express
const express = require('express');
// khai báo middleware
const {
    getAllOrderMiddleware,
    getOrderMiddleware,
    postOrderMiddleware,
    putOrderMiddleware,
    deleteOrderMiddleware
} = require('../middleware/orderMiddleware');

// tạo ra Router drink
const orderRouter = express.Router();
// Khởi tạo các phương thức middleware drink
orderRouter.get('/orders', getAllOrderMiddleware, (req, res) => {
    res.json({
        message: "Get all Order"
    })
});
// phương thức post
orderRouter.post('/orders', postOrderMiddleware, (req, res) => {
    res.json({
        message: "Create a Order"
    })
});
// phương thức get drink by Id
orderRouter.get('/orders/:orderId', getOrderMiddleware, (req, res) => {
    let orderId = req.params.orderId;
    res.json({
        message: `Get a Order by Id = ${orderId}`
    })
});
// phương thức put drink by Id
orderRouter.put('/orders/:orderId', putOrderMiddleware, (req, res) => {
    let orderId = req.params.orderId;
    res.json({
        message: `Update a Order by Id = ${orderId}`
    })
});
// phương thức delete drink by Id
orderRouter.delete('/orders/:orderId', deleteOrderMiddleware, (req, res) => {
    let orderId = req.params.orderId;
    res.json({
        message: `Delete a Order by Id = ${orderId}`
    })
});
module.exports = { orderRouter };