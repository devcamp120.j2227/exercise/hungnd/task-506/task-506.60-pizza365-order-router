// khai báo thư viện express
const express = require('express');
// khai báo middleware
const {
    getAllDrinkMiddleware,
    getDrinkMiddleware,
    postDrinkMiddleware,
    putDrinkMiddleware,
    deleteDrinkMiddleware
} = require('../middleware/drinkMiddleware');

// tạo ra Router drink
const drinkRouter = express.Router();
// Khởi tạo các phương thức middleware drink
drinkRouter.get('/drinks', getAllDrinkMiddleware, (req, res) => {
    res.json({
        message: "Get all drink"
    })
});
// phương thức post
drinkRouter.post('/drinks', postDrinkMiddleware, (req, res) => {
    res.json({
        message: "Create a drink"
    })
});
// phương thức get drink by Id
drinkRouter.get('/drinks/:drinkId', getDrinkMiddleware, (req, res) => {
    let drinkId = req.params.drinkId;
    res.json({
        message: `Get a drink by Id = ${drinkId}`
    })
});
// phương thức put drink by Id
drinkRouter.put('/drinks/:drinkId', putDrinkMiddleware, (req, res) => {
    let drinkId = req.params.drinkId;
    res.json({
        message: `Update a drink by Id = ${drinkId}`
    })
});
// phương thức delete drink by Id
drinkRouter.delete('/drinks/:drinkId', deleteDrinkMiddleware, (req, res) => {
    let drinkId = req.params.drinkId;
    res.json({
        message: `Delete a drink by Id = ${drinkId}`
    })
});
module.exports = { drinkRouter };

