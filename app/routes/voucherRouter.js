// Import thư viện Express Js
const express = require("express");
// khai báo các middleware voucher
const {
    getAllVoucherMiddleware,
    getVoucherMiddleware,
    postVoucherMiddleware,
    putVoucherMiddleware,
    deleteVoucherMiddleware
} = require('../middleware/voucherMiddleware');

// tạo ra voucher router
const voucherRouter = express.Router();

// Khởi tạo các phương thức với middle voucher
voucherRouter.get('/vouchers', getAllVoucherMiddleware, (req, res) => {
    res.json({
        message: "Get all voucher"
    })
});
// phương thức post
voucherRouter.post('/vouchers', postVoucherMiddleware, (req, res) => {
    res.json({
        message: "Create a voucher"
    })
});
// phương thức get drink by Id
voucherRouter.get('/vouchers/:voucherId', getVoucherMiddleware, (req, res) => {
    let voucherId = req.params.voucherId;
    res.json({
        message: `Get a voucher by Id = ${voucherId}`
    })
});
// phương thức put drink by Id
voucherRouter.put('/vouchers/:voucherId', putVoucherMiddleware, (req, res) => {
    let voucherId = req.params.voucherId;
    res.json({
        message: `Update a voucher by Id = ${voucherId}`
    })
});
// phương thức delete drink by Id
voucherRouter.delete('/vouchers/:voucherId', deleteVoucherMiddleware, (req, res) => {
    let voucherId = req.params.voucherId;
    res.json({
        message: `Delete a voucher by Id = ${voucherId}`
    })
});
module.exports = { voucherRouter };